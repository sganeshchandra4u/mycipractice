
# Python program to explain os.mkdir() method
  
# importing os module
import os
  
# Directory
directory = "RESULTS"
  
# Parent Directory path
parent_dir = "./"
  
# Path
path = os.path.join(parent_dir, directory)
  
# Create the directory
# 'GeeksForGeeks' in
# '/home / User / Documents'
#os.mkdir(path)

f = open("./RESULTS/demofile3.txt", "a")
for i in range(40):
    f.write("*"*i+"\n")
f.close()

#open and read the file after the appending:
f = open("demofile2.txt", "r")
print(f.read())
